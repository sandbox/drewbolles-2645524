# Gotham
A simple starting theme for Drupal 8. Utilizes Classy as a base theme, SMACSS, OOCSS, and BEM methodologies. It includes [Susy](http://susy.oddbird.net/) and [Sass MQ](https://github.com/sass-mq/sass-mq) for layout and media query management.

## Requirements
1. Drupal 8
* NPM
* Bower

## Install
Download Gotham and install as a contributed theme (`path/to/site/themes`). Navigate to `path/to/site/themes/gotham/assets/src` and run the commands `$ npm i` and `$ bower install`. Once everything installs run `$ gulp` from the same directory. Gulp will watch your Sass directory and compile your CSS for you.
